# [Programming Foundations: Test-Driven Development](https://www.lynda.com/Developer-Programming-Foundations-tutorials/Foundations-Programming-Test-Driven-Development/124398-2.html)

## Introduction

* TDD in a nutshell
    1. Write a test
    2. Watch the test fail
    3. Write application logic - as simple as possible
    4. Pass the test
    5. Refactor, removing duplication
    6. Pass the test again

## Chapter 01 - Introduction to TDD
* This course is about automated unit test
* TDD asks developer to write the test first before writing code

## Chapter 02 - Getting Started

### Using unit testing frameworks
* Reference: https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks#C.2B.2B

### Understanding assertions
* Assertion is to say something to be true
* If an assertion fails, then our code is broken, there's something wrong and we have to fix it

## Chapter 03 - Working with tests

### The process of TDD revisited
* A quick summary of the TDD process
    * Red: make a task that fails
    * Green: make that task pass
    * Grey: refactor, make it right
* This isn't done once, this is a cycle. We repeat that throughout the lifetime of our project

## Chapter 04 - Individual Techniques

* Each individual test can typically be summarized using three words
    * Arrange: get something set up to manipulate
    * Act: change it, we do something to it
    * Assert: check the result, check it worked

### Creating a test for expected exceptions
* Setup: What runs before each test
* Teardown: What runs after each test
* Setup Before Class: Run once before all the tests
* Teardown After Class: Run once after all the tests

## Chapter 05 - Addtional Topics

### Introducing mock objects
* Reasons for Mock Objects
    * Real object hasn't been written yet
    * What we're calling hass a UI / needs human interaction
    * Slow or difficult to set up
    * External resource: file system, databse, network, printer
    * Non-deterministic behavior

### Measuring code coverage
* A percentage measurement of exactly how much of the code is being successfully hit by the unit tests.

### TDD recommendations
* Numbers
    * One test case / test fixture for each Class
    * 3 - 5 test methods for each class method
* What to test
    * A test for every branch
    * "Test until fear turns to boredom"
    * Use code coverage tools
* What to avoid
    * Interact with a database or file system
    * Require non-trivial network communication
    * Require environment changes to run
    * Call comples collaborator objects
    
